<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax extends Controller {
	public function action_getItem()
	{
		$id = $this->request->param('id');
		$result = Model::factory('Catalog')->get_one($id);
		$this->response->body($result[0]['description']);
	}
}