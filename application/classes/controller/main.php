<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Main extends Controller_Template {
	public $template = 'index';
	
	public function before()
    {		
    	$action_name = $this->request->action();
		$controller_name = $this->request->controller();
		
		$menu = Model::factory('Menu')->get_allactive();
		
    	$this->template = View::factory($this->template);
		$this->template->menu = View::factory('menu')
								->bind('menu', $menu)
								->bind('active',  $action_name);
								
		$this->template->smallmenu = View::factory('smallmenu')
									->bind('menu', $menu)
									->bind('active',  $action_name);
									
		$title_add = Model::factory('titles')->get_action($controller_name, $action_name);
		if (!($title_add)) $title_add = Model::factory('titles')->get_controller($controller_name);
		if (!($title_add))
		{
			$title_add[0]['title'] = "Неизвестная";
			$title_add[0]['description'] = "Неизвестная";
		}
									
        $this->template->title = $title_add[0]['title'].Model::factory('Settings')->get_one('seperator').Model::factory('Settings')->get_one('title');
		$this->template->description = $title_add[0]['description']; 
		$this->template->phone = Model::factory('Settings')->get_one('phone');
    }
	
	public function action_login()
	{
		$form = View::factory('admin/loginform')
				->bind('message', $message);;
		$this->template->content = View::factory('onecolumn')
									->bind('content', $form);
	}
	
    public function action_auth() 
    {
    	$form = View::factory('admin/loginform')
            ->bind('message', $message);
        $this->template->content = View::factory('onecolumn')
									->bind('content', $form);
             
        if (HTTP_Request::POST == $this->request->method()) 
        {
            // Attempt to login user
            $remember = array_key_exists('remember', $this->request->post()) ? (bool) $this->request->post('remember') : FALSE;
            $user = Auth::instance()->login($this->request->post('username'), $this->request->post('password'), $remember);
             
            // If successful, redirect user
            if ($user) 
            {
                Request::current()->redirect('admin/index');
            } 
            else
            {
                $message = 'Не удалось войти';
            }
        }
    }
     
    public function action_logout() 
    {
        // Log user out
        Auth::instance()->logout();
         
        // Redirect to login page
        Request::current()->redirect('login');
    }
	
	
	public function action_index()
	{
		$this->template->content = View::factory('onecolumn')
									->bind('content', $content); 
		$content = Model::factory('Static')->get_text(3);
	}
	
	public function action_catalog()
	{
		$id = $this->request->param('id');
		
		$catalog = Model::factory('catalog')->get_all();
		$leftside = View::factory('items')
						->bind('catalog', $catalog);
					
		if (!empty($id))
		{
			$item = Model::factory('catalog')->get_one($id);
			$arr = explode('|', $item[0]['characteristics']);
			//print_r(array_values($arr));
			for ($i=0;$i<count($arr);$i++)
			{
				$line = explode('@', $arr[$i]);
				if (empty($line[0]) || empty($line[1])) break; 
				$char = Model::factory('characteristics')->get_one($line[0]);
				$fortable[$i]['key'] = $char;
				$fortable[$i]['value'] = $line[1];
			}
			$table = View::factory('table')
						->bind('characteristics', $fortable);	
			$rightside = View::factory('bigitem')
							->bind('title', $item[0]['title'])
							->bind('description', $item[0]['description'])
							->bind('img', $item[0]['img'])
							->bind('table', $table);	
		}			
		
		$this->template->content = View::factory('twocolumn')
									->bind('leftside', $leftside)
									->bind('rightside', $rightside);
						 
	}
	
	public function action_contacts()
	{
		$this->template->content = View::factory('twocolumn')
									->bind('leftside', $leftside)
									->bind('rightside', $rightside); 
							
		$leftside = Model::factory('Static')->get_text(1);
		$rightside = Model::factory('Static')->get_text(2);
	}
	
	public function action_partners()
	{
		$this->template->content = View::factory('onecolumn')
									->bind('content', $rightside); 
		//$leftside = Model::factory('Static')->get_text(5);
		$rightside = Model::factory('Static')->get_text(6);
	}
	
	public function action_about()
	{
		$this->template->content = View::factory('onecolumn')
									->bind('content', $content); 
		
		$content = Model::factory('Static')->get_text(4);
	}

	public function action_admin()
	{
		Request::current()->redirect('/admin/index');
	}
	
	public function after()
    {
     	$this->response->body($this->template);
    }

}