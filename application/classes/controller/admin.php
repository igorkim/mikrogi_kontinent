<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin extends Controller_Template {
	public function before()
    {
    	$action_name = $this->request->action();
		$controller_name = $this->request->controller();
		
    	$this->template = View::factory('admin/index');
									
		$title_add = Model::factory('titles')->get_action($controller_name, $action_name);
		if (!($title_add)) $title_add = Model::factory('titles')->get_controller($controller_name);
		if (!($title_add))
		{
			$title_add[0]['title'] = "Неизвестная";
			$title_add[0]['description'] = "Неизвестная";
		}
					
		//Check for Auth				
        $this->template->title = $title_add[0]['title'].Model::factory('Settings')->get_one('seperator').Model::factory('Settings')->get_one('title');

        $user = Auth::instance()->get_user();
		
        if (!$user)
        {
            Request::current()->redirect('login');
        }
    }
	
	public function action_create() 
    {
		$rightside = View::factory('admin/usercreate')
           	 	->bind('errors', $errors)
            	->bind('message', $message);
									
		$this->template->content = View::factory('twocolumn')
									->bind('leftside', $leftside)
									->bind('rightside', $rightside); 
		$leftside = View::factory('admin/leftside');
             
        if (HTTP_Request::POST == $this->request->method()) 
        {           
            try {
                $user = ORM::factory('user')->create_user($this->request->post(), array(
                    'username',
                    'password',
                    'email'            
                ));
				
                $user->add('roles', ORM::factory('role', array('name' => 'login')));

                $_POST = array();
                 
                $message = "Вы добавили пользователя '{$user->username}'";
				Request::current()->redirect('admin/users');
                 
            } catch (ORM_Validation_Exception $e) {
                $message = 'Произошла ошибка.';
                $errors = $e->errors('models');
            }
        }
    }

	public function action_delete()
	{
		$user_name = $this->request->param('id');
		$user = ORM::factory('user', $user_name); 
		
		if ($user->loaded()) $user->delete();	
		
		Request::current()->redirect('admin/users/');
	}	
	
	public function action_index()
	{
		$this->template->content = View::factory('twocolumn')
									->bind('leftside', $leftside)
									->bind('rightside', $rightside); 
		$leftside = View::factory('admin/leftside');
		$rightside = "Выберите нужный пункт из меню слева!";
	}
	
	public function action_users()
	{
		$users = Model::factory('Users')->get_all();
		$rightside = View::factory('admin/users')
						->bind('users', $users);
		
		$leftside = View::factory('admin/leftside');
		$this->template->content = View::factory('twocolumn')
									->bind('leftside', $leftside)
									->bind('rightside', $rightside); 
	}
	
	public function action_menu()
	{
		$menu = Model::factory('Menu')->get_all();
		$rightside = View::factory('admin/menu')
						->bind('menu', $menu);
		
		$leftside = View::factory('admin/leftside');
		$this->template->content = View::factory('twocolumn')
									->bind('leftside', $leftside)
									->bind('rightside', $rightside); 
	}
	
	public function action_settings()
	{
		$settings = Model::factory('Settings')->get_all();
		$rightside = View::factory('admin/settings')
						->bind('settings', $settings);;
		
		$leftside = View::factory('admin/leftside');
		$this->template->content = View::factory('twocolumn')
									->bind('leftside', $leftside)
									->bind('rightside', $rightside); 
	}
	
	public function action_static()
	{
		$static = Model::factory('static')->get_all();
		$rightside = View::factory('admin/static')
						->bind('static', $static);;
		
		$leftside = View::factory('admin/leftside');
		$this->template->content = View::factory('twocolumn')
									->bind('leftside', $leftside)
									->bind('rightside', $rightside); 
	}
	
	public function action_catalog()
	{
		$characteristics = Model::factory('characteristics')->get_all();
		$catalog = Model::factory('catalog')->get_all();
		
		$rightside = View::factory('admin/catalog')
						->bind('catalog', $catalog)
						->bind('characteristics', $characteristics);
		
		$leftside = View::factory('admin/leftside');
		$this->template->content = View::factory('twocolumn')
									->bind('leftside', $leftside)
									->bind('rightside', $rightside); 
	}
	
	public function action_titles()
	{
		$titles = Model::factory('titles')->get_all();
		$rightside = View::factory('admin/titles')
						->bind('titles', $titles);;
		
		$leftside = View::factory('admin/leftside');
		$this->template->content = View::factory('twocolumn')
									->bind('leftside', $leftside)
									->bind('rightside', $rightside); 
	}
	
	public function action_characteristic()
	{
		$characteristics = Model::factory('characteristics')->get_all();
		$rightside = View::factory('admin/characteristics')
						->bind('characteristics', $characteristics);;
		
		$leftside = View::factory('admin/leftside');
		$this->template->content = View::factory('twocolumn')
									->bind('leftside', $leftside)
									->bind('rightside', $rightside); 
	}
	
	public function action_addItem()
	{
		Model::factory('Catalog')->put_one($_POST);
		Request::current()->redirect('admin/catalog/');
	}

	public function action_editItem()
	{
		Model::factory('Catalog')->edit_one($_POST);
		Request::current()->redirect('admin/catalog/');
	}
	
	public function action_deleteItem()
	{
		$id = $this->request->param('id');
		Model::factory('Catalog')->delete_one($id);
		
		Request::current()->redirect('admin/catalog/');
	}	

	public function action_addStatic()
	{
		Model::factory('Static')->put_one($_POST);
		Request::current()->redirect('admin/static/');
	}

	public function action_editStatic()
	{
		Model::factory('Static')->edit_one($_POST);
		Request::current()->redirect('admin/static/');
	}

	public function action_addTitle()
	{
		Model::factory('titles')->put_one($_POST);
		Request::current()->redirect('admin/titles/');
	}

	public function action_editTitle()
	{
		Model::factory('titles')->edit_one($_POST);
		Request::current()->redirect('admin/titles/');
	}

	public function action_addCharacteristic()
	{
		Model::factory('Characteristics')->put_one($_POST);
		Request::current()->redirect('admin/characteristic/');
	}

	public function action_editCharacteristic()
	{
		Model::factory('Characteristics')->edit_one($_POST);
		Request::current()->redirect('admin/characteristic/');
	}
	
	public function action_editsetting()
	{
		Model::factory('Settings')->put_one($_POST);
		Request::current()->redirect('admin/settings/');
	}
	
	public function action_editMenu()
	{
		Model::factory('Menu')->edit_menu($_POST);
		Request::current()->redirect('admin/menu/');
	}
}