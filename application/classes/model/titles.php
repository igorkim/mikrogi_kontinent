<?php defined('SYSPATH') or die('No direct script access.');

class Model_Titles extends Model
{
    protected $_table = 'titles';
 
    /**
     * Get all articles
     * @return array
     */
     
    public function get_all()
    {
        $sql = "SELECT * FROM ". $this->_table;
 
        $result = DB::query(Database::SELECT, $sql)->execute()->as_array();
		
		return $result;
    }
	
    public function get_controller($controller)
    {
		$sql = "SELECT * FROM ". $this->_table." WHERE controller = '" .$controller ."' AND action = '' AND active > 0";
 
        return DB::query(Database::SELECT, $sql)->execute()->as_array();
    }
	
    public function get_action($controller, $action)
    {
		$sql = "SELECT * FROM ". $this->_table." WHERE controller = '" .$controller ."' AND action = '" .$action."' AND active > 0";
 
        return DB::query(Database::SELECT, $sql)->execute()->as_array();
    }
	
    public function put_one($arr)
    {
		$sql = "INSERT INTO ". $this->_table." (id, controller, action, title, description, active) ".
				" VALUES (0, '". Arr::get($arr, 'controller'). "', '". Arr::get($arr, 'action'). "',
				'". Arr::get($arr, 'title'). "', '". Arr::get($arr, 'description'). "', 
				'". Arr::get($arr, 'active'). "')";
 
        return DB::query(Database::INSERT, $sql)->execute();
    }	
	
    public function edit_one($arr)
    {
		$sql = "UPDATE ". $this->_table." SET ".
				"controller = '". Arr::get($arr, 'controller'). "', ". 
				"action = '". Arr::get($arr, 'action'). "', ". 
				"title = '". Arr::get($arr, 'title'). "', ". 
				"description = '". Arr::get($arr, 'description'). "', ".
				"active = '". Arr::get($arr, 'active'). "' ".
				" WHERE id = '". Arr::get($arr, 'id'). "'";
 
        return DB::query(Database::UPDATE, $sql)->execute();
    }	
}