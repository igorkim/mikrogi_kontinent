<?php defined('SYSPATH') or die('No direct script access.');

class Model_Settings extends Model
{
    protected $_table = 'settings';
	protected $_whatSearch;
 
    /**
     * Get all articles
     * @return array
     */
     
    public function get_all()
    {
        $sql = "SELECT * FROM ". $this->_table;
 
        $result = DB::query(Database::SELECT, $sql)->execute()->as_array();
		
		return $result;
    }
	
    public function get_one($what)
    {
    	$this->_whatSearch = $what;
        $sql = "SELECT value FROM ". $this->_table. " WHERE name = '". $this->_whatSearch. "'";
 
        $result = DB::query(Database::SELECT, $sql)->execute()->as_array();
		
		return $result[0]['value'];
    }	
	
    public function put_one($arr)
    {
		$sql = "UPDATE ". $this->_table." SET ".
				"value = '". Arr::get($arr, 'value'). "' WHERE name = '". Arr::get($arr, 'name'). "'";
 
        return DB::query(Database::UPDATE, $sql)->execute();
    }	
}