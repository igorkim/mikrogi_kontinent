<?php defined('SYSPATH') or die('No direct script access.');

class Model_Menu extends Model
{
    protected $_table = 'menu';
	protected $_whatSearch;
 
    /**
     * Get all articles
     * @return array
     */
    public function get_allactive()
    {
        $sql = "SELECT * FROM ". $this->_table. " WHERE active = 1 ORDER BY position ASC";
 
        return DB::query(Database::SELECT, $sql)->execute()->as_array();
    }
	
    public function get_all()
    {
        $sql = "SELECT * FROM ". $this->_table. " ORDER BY position ASC";
 
        return DB::query(Database::SELECT, $sql)->execute()->as_array();
    }
	
    public function edit_menu($arr)
    {
        $sql = "UPDATE ". $this->_table." SET ".
				"title = '". Arr::get($arr, 'title'). "', ".
				"link = '". Arr::get($arr, 'link'). "', ".
				"position = '". Arr::get($arr, 'position'). "', ".
				"active = '". Arr::get($arr, 'active'). "', ".
				"action = '". Arr::get($arr, 'action'). "' WHERE id = ". Arr::get($arr, 'id');
 
        return DB::query(Database::UPDATE, $sql)->execute();
    }
}