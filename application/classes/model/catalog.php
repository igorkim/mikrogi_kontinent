<?php defined('SYSPATH') or die('No direct script access.');

class Model_Catalog extends Model
{
    protected $_table = 'catalog';
	protected $_whatSearch;
     
    public function get_all()
    {
        $sql = "SELECT * FROM ". $this->_table;

		return DB::query(Database::SELECT, $sql)->execute();
    }
	
    public function get_one($id)
    {
		$sql = "SELECT * FROM ". $this->_table.
				" WHERE id = '". $id. "'";
 
        return DB::query(Database::SELECT, $sql)->execute()->as_array();
    }
	
    public function put_one($arr)
    {
		$sql = "INSERT INTO ". $this->_table." (id, title, description, characteristics, img, active) ".
				" VALUES (0, '". Arr::get($arr, 'title'). "', '". Arr::get($arr, 'description'). "', 
				'". Arr::get($arr, 'characteristics'). "', '". Arr::get($arr, 'img'). "',
				'". Arr::get($arr, 'active'). "')";
 
        return DB::query(Database::INSERT, $sql)->execute();
    }	
	
    public function edit_one($arr)
    {
		$sql = "UPDATE ". $this->_table." SET ".
				"title = '". Arr::get($arr, 'title'). "', ". 
				"description = '". Arr::get($arr, 'description'). "', ".
				"characteristics = '". Arr::get($arr, 'characteristics'). "', ".  
				"img = '". Arr::get($arr, 'img'). "', ". 
				"active = '". Arr::get($arr, 'active'). "' ". 
				" WHERE id = '". Arr::get($arr, 'id'). "'";
 
        return DB::query(Database::UPDATE, $sql)->execute();
    }	
	
    public function delete_one($id)
    {
		$sql = "DELETE FROM ". $this->_table.
				" WHERE id = '". $id. "'";
 
        DB::query(Database::DELETE, $sql)->execute();
    }	
}