<?php defined('SYSPATH') or die('No direct script access.');

class Model_Users extends Model
{
    protected $_table = 'users';
 
    /**
     * Get all articles
     * @return array
     */
    public function get_all()
    {
        $sql = "SELECT * FROM ". $this->_table;
 
        return DB::query(Database::SELECT, $sql)->execute()->as_array();
    }
}