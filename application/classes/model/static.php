<?php defined('SYSPATH') or die('No direct script access.');

class Model_Static extends Model
{
    protected $_table = 'statics';
	protected $_whatSearch;
 
    /**
     * Get all articles
     * @return array
     */
     
    public function get_all()
    {
        $sql = "SELECT * FROM ". $this->_table;
 
        $result = DB::query(Database::SELECT, $sql)->execute()->as_array();
		
		return $result;
    }
	
    public function put_one($arr)
    {
		$sql = "INSERT INTO ". $this->_table." (id, title, text, active) ".
				" VALUES (0, '". Arr::get($arr, 'title'). "', '". Arr::get($arr, 'text'). "',
				'". Arr::get($arr, 'active'). "')";
 
        return DB::query(Database::INSERT, $sql)->execute();
    }	
	
    public function edit_one($arr)
    {
		$sql = "UPDATE ". $this->_table." SET ".
				"title = '". Arr::get($arr, 'title'). "', ". 
				"text = '". Arr::get($arr, 'text'). "', ". 
				"active = '". Arr::get($arr, 'active'). "' ". 
				" WHERE id = '". Arr::get($arr, 'id'). "'";
 
        return DB::query(Database::UPDATE, $sql)->execute();
    }	
	
    public function get_text($id)
    {
		$sql = "SELECT text FROM ". $this->_table." WHERE id = '". $id. "'";
 
        $result = DB::query(Database::SELECT, $sql)->execute();
		
		return $result[0]['text'];
    }	
}