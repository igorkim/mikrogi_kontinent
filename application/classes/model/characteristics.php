<?php defined('SYSPATH') or die('No direct script access.');

class Model_Characteristics extends Model
{
    protected $_table = 'characteristics';
	protected $_whatSearch;
 
    /**
     * Get all articles
     * @return array
     */
     
    public function get_all()
    {
        $sql = "SELECT * FROM ". $this->_table;
 
        return DB::query(Database::SELECT, $sql)->execute()->as_array();
    }
	
    public function get_one($id)
    {
        $sql = "SELECT name FROM ". $this->_table. 
				" WHERE id = ". $id;
 
        $result = DB::query(Database::SELECT, $sql)->execute()->as_array();
		return $result[0]['name'];
    }
	
    public function put_one($arr)
    {
		$sql = "INSERT INTO ". $this->_table." (id, name) ".
				" VALUES (0, '". Arr::get($arr, 'name'). "')";
 
        return DB::query(Database::UPDATE, $sql)->execute();
    }	
	
    public function edit_one($arr)
    {
		$sql = "UPDATE ". $this->_table." SET ".
				"name = '". Arr::get($arr, 'name'). "' WHERE id = '". Arr::get($arr, 'id'). "'";
 
        return DB::query(Database::UPDATE, $sql)->execute();
    }	
}