<? foreach ($catalog as $element): ?>
<a href="/catalog/<?=$element['id']?>">
<div class="item">
	<!-- Product Picture -->
	<? if ($element['img'] == ''): ?>
	<img src="/img/product.png"/>
	<? else: ?>
	<img src="/img/<?=$element['img']?>"/>
	<? endif; ?>
	<!-- Product Title -->
	<div class="name"><?=$element['title']?></div>
	<!-- Product Description -->
	<div class="description">&nbsp;</div>
</div>
</a>
<? endforeach; ?>