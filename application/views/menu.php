<? 
	$total = count($menu); 
	$counter = 0;
?>
<div class="row menu">

	<div class="menuwrapper"></div>
	<? foreach ($menu as $element): ?>
		<? $counter++; ?>
		
		<? if ($element['action'] == $active): ?>
			<div class="menuelement active"><a href="<?=$element['link']?>"><?=$element['title']?></a></div>
		<? else: ?>
			<div class="menuelement"><a href="<?=$element['link']?>"><?=$element['title']?></a></div>
		<? endif; ?>
		
		<? if ($counter < $total): ?>
			<div class="menuimgwrapper"></div>
		<? endif; ?>
		
	<? endforeach; ?>
</div>