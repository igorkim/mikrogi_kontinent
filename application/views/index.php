<!DOCTYPE html>

<head>
	<!-- Encoding, default is "utf-8", you can use windows-1251 if need -->
	<meta charset="utf-8">
	
	<meta name="description" content="<?=$description ?>" />
	
	<!-- Twitter Bootstrap CSS -->
	<link rel="stylesheet" href="/css/bootstrap.css" type="text/css" />
	<!-- Main page CSS -->
	<link rel="stylesheet" href="/css/main.css" type="text/css" />
	<!-- HTML5 support for IE -->
	<link rel="stylesheet" href="/js/google-code-prettify/prettify.css" type="text/css" />
	
	<!-- Page title -->
	<title><?=$title ?></title>
</head>


<body>

	<div class="container">
		
		<!-- Header -->
		<div class="row head">
			<!-- Left Shadow Wrapper -->
			<div class="span verticalwrapper"></div>
			
			<div class="span4 logospan">
				<!-- First left header -->
				<a href="/index/">
					<div class="row logorow1">
						<!-- Curve Image, image is selected in CSS -->
						<div class="shape"></div>
					</div>
				</a>
				<!-- Second Left header -->
				<div class="row logorow2">
					Тел.: <?=$phone?>
				</div>
			</div>
			
			<!-- Center shadow wrapper -->
			<div class="span verticalminiwrapper"></div>
			
			<!-- Right Img Header -->
			<div class="span8 logospanr"></div>
			
			<!-- Right shadow wrapper -->
			<div class="span verticalminiwrapper"></div>
		</div>
		
		<!-- Main header menu -->
		<?=$menu ?>
		
		<div class="row content">
			<!-- Left Shadow Wrapper -->
			<div class="span verticalwrapper"></div>
			
			<!-- Main content, two column view in twocolumn.html -->
			<?=$content ?>
			
			<!-- Right shadow wrapper -->
			<div class="span verticalminiwrapper"></div>
		</div>
		
		<!-- Footer -->
		<div class="row menu">
			<!-- First menu wrapper -->
			<div class="menuwrapper"></div>
			<!-- Copyright text -->
			<div class="bottomelement"><a href="/admin/index/">©</a> создание сайта <a target="_blank" href="http://provectus.ru/">Provectus</a></div>
			<!-- Copyright text -->
			<div class="menumegawrapper"></div>
			
			<!-- Bottom small menu -->
			<!-- You can add class "active" to div element for make link active -->
			<?=$smallmenu ?>
		</div>
	</div>
	
	<!-- jQuery Library -->
    <script type="text/javascript" src="/js/jquery.js"></script>
    <!-- HTML5 support for IE -->
    <script type="text/javascript" src="/js/google-code-prettify/prettify.js"></script>
    <!-- 
    	Twitter Bootstrap Modules 
    	You can delete unused
    -->
    <script type="text/javascript" src="/js/bootstrap-transition.js"></script>
    <script type="text/javascript" src="/js/bootstrap-alert.js"></script>
    <script type="text/javascript" src="/js/bootstrap-modal.js"></script>
    <script type="text/javascript" src="/js/bootstrap-dropdown.js"></script>
    <script type="text/javascript" src="/js/bootstrap-scrollspy.js"></script>
   	<script type="text/javascript" src="/js/bootstrap-tab.js"></script>
    <script type="text/javascript" src="/js/bootstrap-tooltip.js"></script>
    <script type="text/javascript" src="/js/bootstrap-popover.js"></script>
    <script type="text/javascript" src="/js/bootstrap-button.js"></script>
    <script type="text/javascript" src="/js/bootstrap-collapse.js"></script>
   	<script type="text/javascript" src="/js/bootstrap-carousel.js"></script>
    <script type="text/javascript" src="/js/bootstrap-typeahead.js"></script>
</body>
<!-- 
	For technical support mail: contact@mikrogi.com
-->
