<!DOCTYPE html>

<head>
	<!-- Encoding, default is "utf-8", you can use windows-1251 if need -->
	<meta charset="utf-8">
	
	<!-- Twitter Bootstrap CSS -->
	<link rel="stylesheet" href="/css/bootstrap.css" type="text/css" />
	<!-- Main page CSS -->
	<link rel="stylesheet" href="/css/main.css" type="text/css" />
	<!-- HTML5 support for IE -->
	<link rel="stylesheet" href="/js/google-code-prettify/prettify.css" type="text/css" />
	
	<!-- Page title -->
	<title><?=$title ?></title>
</head>


<body>

	<div class="container">
		
		<!-- Main header menu -->
		<div class="row menu">
			<div class="menuwrapper"></div>
			<div class="menuelement"><a target="_blank" href="/index/">На сайт</a></div>
			<div class="menuimgwrapper"></div>
			<div class="menuelement"><a href="/logout/">Выход</a></div>
		</div>
		
		<div class="row content">
			<!-- Left Shadow Wrapper -->
			<div class="span verticalwrapper"></div>
			
			<!-- Main content, two column view in twocolumn.html -->
			<?=$content ?>
			
			<!-- Right shadow wrapper -->
			<div class="span verticalminiwrapper"></div>
		</div>
		
		<!-- Footer -->
		<div class="row menu">
			<!-- First menu wrapper -->
			<div class="menuwrapper"></div>
			<!-- Copyright text -->
			<div class="bottomelement">© создание сайта <a target="_blank" href="http://provectus.ru/">Provectus</a></div>
			<!-- Copyright text -->
			<div class="menumegawrapper"></div>
		</div>
	</div>
	
	<!-- jQuery Library -->
    <script type="text/javascript" src="/js/jquery.js"></script>
    <!-- HTML5 support for IE -->
    <script type="text/javascript" src="/js/google-code-prettify/prettify.js"></script>
    <!-- 
    	Twitter Bootstrap Modules 
    	You can delete unused
    -->
    <script type="text/javascript" src="/js/bootstrap-transition.js"></script>
    <script type="text/javascript" src="/js/bootstrap-alert.js"></script>
    <script type="text/javascript" src="/js/bootstrap-modal.js"></script>
    <script type="text/javascript" src="/js/bootstrap-dropdown.js"></script>
    <script type="text/javascript" src="/js/bootstrap-scrollspy.js"></script>
   	<script type="text/javascript" src="/js/bootstrap-tab.js"></script>
    <script type="text/javascript" src="/js/bootstrap-tooltip.js"></script>
    <script type="text/javascript" src="/js/bootstrap-popover.js"></script>
    <script type="text/javascript" src="/js/bootstrap-button.js"></script>
    <script type="text/javascript" src="/js/bootstrap-collapse.js"></script>
   	<script type="text/javascript" src="/js/bootstrap-carousel.js"></script>
    <script type="text/javascript" src="/js/bootstrap-typeahead.js"></script>
</body>
<!-- 
	For technical support mail: contact@mikrogi.com
-->
