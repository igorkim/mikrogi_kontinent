<ul class="nav nav-pills nav-stacked leftmenu">
	<li class="active" ><a href="/admin/settings/">Настройки</a><br/></li>
	<li class="active" ><a href="/admin/users/">Администраторы</a><br/></li>
	<li class="active" ><a href="/admin/titles/">Заголовки страниц</a><br/></li>
	<li class="active" ><a href="/admin/menu/">Меню</a><br/></li>
	<li class="active" ><a href="/admin/catalog/">Каталог</a><br/></li>
	<li class="active" ><a href="/admin/characteristic/">Характеристики товаров</a><br/></li>
	<li class="active" ><a href="/admin/static/">Статические страницы</a><br/></li>
</ul>