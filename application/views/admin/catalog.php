<script>
	function edit_item(id) 
	{
		
		
		$('#id').attr('value', id);
		$('#title').attr('value', $('#title'+id).html());
		$('#active').attr('value', $('#active'+id).html());
		$('#characteristics').attr('value', $('#characteristics'+id).html());
		$('#img').attr('value', $('#img'+id).html());
		//$('#description').html($('#description'+id).html());
		
    	$.ajax({
        	type: 'GET',
       	 	url: '/ajax/getItem/'+id+'/',
        	cache: false,   
        	success: function(html){
            	$('#description').html(html);
            	
        	}
    	});  
		
		
		$('#formModal').attr('action', '/admin/editItem/');
		$('#btnModal').html('Сохранить');
		$('#myModal').modal();
	}
	
	function add_item() 
	{
		$('#formModal').attr('action', '/admin/addItem/');
		$('#btnModal').html('Добавить');
		$('#myModal').modal();
	}
</script>

<div class="modal hide" id="myModal">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">×</button>
    <h3>Редактировать товар</h3>
  </div>
  <form id="formModal" action="" method="post">
  	<div class="modal-body">
  		<input id="id" name="id" type="hidden">
  		<label>Заголовок</label>
  		<input id="title" name="title" type="text" class="span3">
  		<label>Активность (0/1)</label>
  		<input id="active" name="active" type="text" class="span3">
  		<label>Изображение</label>
  		<input id="img" name="img" type="text" class="span3">
  		<label>Описание</label>
  		<textarea rows="4" id="description" name="description" class="input-xlarge"></textarea>
  		<label>Характеристики</label>
  		<label>Вводите в виде: "ID параметра@значение|"</label>
  		<div id="characteristics_items">
  			<? if ($characteristics): ?>
			<? foreach($characteristics as $characteristic): ?>
				<?=$characteristic['id']?>-<?=$characteristic['name']?><br/>
			<? endforeach; ?>
			<? endif; ?>
  		</div>
  		<input id="characteristics" name="characteristics" type="text" class="span3">
 	</div>
  	<div class="modal-footer">
    	<button class="btn" data-dismiss="modal">Отмена</a>
    	<button id="btnModal" type="submit" class="btn btn-primary">Сохранить</button>
  	</div>
  </form>
</div>

<h3>Каталог товаров</h3>
<? if (count($catalog)>0): ?>
<table class="table">
	<thead>
    	<tr>
    		<th>ID</th>
      		<th>Заголовок</th>
      		<th>Активность</th>
      		<th>Удалить</th>
    	</tr>
  	</thead>
  	
  	<tbody>
  		<? foreach ($catalog as $element): ?>
    	<tr>
      		<td onclick="edit_item(<?=$element['id']?>); return false"><?=$element['id']?></td>
      		<td onclick="edit_item(<?=$element['id']?>); return false" id="title<?=$element['id']?>"><?=$element['title']?></td>
      		<td onclick="edit_item(<?=$element['id']?>); return false" id="active<?=$element['id']?>"><?=$element['active']?></td>
      		<td><a href="/admin/deleteItem/<?=$element['id']?>/">Удалить</a></td>
      		
      		<!--<div id="description<?=$element['id']?>" style="display:none;"><?=$element['description']?></div>-->
      		<div id="characteristics<?=$element['id']?>" class="hide"><?=$element['characteristics']?></div>
      		<div id="img<?=$element['id']?>" class="hide"><?=$element['img']?></div>
   	 	</tr>
   	 	<? endforeach; ?>
  	</tbody>
</table>
<? endif; ?>

<div class="span6">
	<a onclick="add_item(); return false">Добавить товар</a><br/><br/>
	Подсказка: нажмите на строку для редактирования.
</div>
