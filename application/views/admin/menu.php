<script>
	function edit_menu(id) 
	{
		$('#myModal').modal();
		$('#title').attr('value', $('#title'+id).html());
		$('#link').attr('value', $('#link'+id).html());
		$('#position').attr('value', $('#position'+id).html());
		$('#active').attr('value', $('#active'+id).html());
		$('#action').attr('value', $('#action'+id).html());
		$('#id').attr('value', id);
	}
</script>

<div class="modal hide" id="myModal">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">×</button>
    <h3>Редактировать меню</h3>
  </div>
  <form action="/admin/editMenu/" method="post">
  	<div class="modal-body">
  		<input id="id" name="id" type="hidden">
  		<label>Заголовок</label>
  		<input id="title" name="title" type="text" class="span3">
  		<label>Ссылка</label>
  		<input id="link" name="link" type="text" class="span3">
  		<label>Позиция (Целое число)</label>
  		<input id="position" name="position" type="text" class="span3">
  		<label>Активность (0/1)</label>
  		<input id="active" name="active" type="text" class="span3">
  		<label>Раздел</label>
  		<input id="action" name="action" type="text" class="span3">
 	</div>
  	<div class="modal-footer">
    	<button class="btn" data-dismiss="modal">Отмена</a>
    	<button type="submit" class="btn btn-primary">Сохранить</a>
  	</div>
  </form>
</div>

<h3>Управление главным меню</h3>
<table class="table">
	<thead>
    	<tr>
      		<th>Заголовок</th>
      		<th>Ссылка</th>
      		<th>Позиция</th>
      		<th>Активность</th>
      		<th>Действие</th>
    	</tr>
  	</thead>
  	
  	<tbody>
  		<? foreach ($menu as $element): ?>
    	<tr onclick="edit_menu(<?=$element['id']?>); return false">
      		<td id="title<?=$element['id']?>"><?=$element['title']?></td>
      		<td id="link<?=$element['id']?>"><?=$element['link']?></td>
      		<td id="position<?=$element['id']?>"><?=$element['position']?></td>
      		<td id="active<?=$element['id']?>"><?=$element['active']?></td>
      		<td id="action<?=$element['id']?>"><?=$element['action']?></td>
   	 	</tr>
   	 	<? endforeach; ?>
  	</tbody>
</table>

<div class="span6">
	Подсказка: нажмите на строку для редактирования.
</div>
