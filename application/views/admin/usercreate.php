<div class="span6">
<h3>Добавление администратора</h3>
<?= Form::open('/admin/create'); ?>
 
<?= Form::label('username', 'Имя пользователя'); ?>
<?= Form::input('username', HTML::chars(Arr::get($_POST, 'username'))); ?>
<? if (Arr::get($errors, 'username')): ?>
<div class="alert alert-error">
    <?= Arr::get($errors, 'username'); ?>
</div>
<? endif; ?>
 
<?= Form::label('email', 'Email'); ?>
<?= Form::input('email', HTML::chars(Arr::get($_POST, 'email'))); ?>

<? if (Arr::get($errors, 'email')): ?>
<div class="alert alert-error">
    <?= Arr::get($errors, 'email'); ?>
</div>
<? endif; ?>

<?= Form::label('password', 'Пароль'); ?>
<?= Form::password('password'); ?>
<? if (Arr::path($errors, '_external.password')): ?>
<div class="alert alert-error">
    <?= Arr::path($errors, '_external.password'); ?>
</div>
<? endif; ?>

<?= Form::label('password_confirm', 'Подтвердите пароль'); ?>
<?= Form::password('password_confirm'); ?>

<? if (Arr::path($errors, '_external.password_confirm')): ?>
<div class="alert alert-error">
    <?= Arr::path($errors, '_external.password_confirm'); ?>
</div>
<? endif; ?>

<br/>

<?= Form::submit('create', 'Добавить пользователя'); ?>
<?= Form::close(); ?>

</div>