<h3>Характеристики товаров</h3>
<? if ($characteristics): ?>
<? foreach($characteristics as $characteristic): ?>
<form action="/admin/editCharacteristic" method="post" class="form-horizontal">
	<fieldset>
    	<div class="control-group">
      		<label class="control-label" for="<?=$characteristic['id']?>"><?=$characteristic['id']?></label>
      		<div class="controls">
      			<input type="hidden" class="input-xlarge" name="id" value="<?=$characteristic['id']?>">
        		<input value="<?=$characteristic['name']?>" type="text" class="input-xlarge" name="name" id="<?=$characteristic['id']?>">
       			<button type="submit" class="btn btn-primary">Сохранить</button>
     		</div>
    	</div>
  	</fieldset>
</form>
<? endforeach; ?>
<? endif; ?>

<form action="/admin/addCharacteristic" method="post" class="form-horizontal">
	<fieldset>
    	<div class="control-group">
      		<label class="control-label" for="add">Параметр</label>
      		<div class="controls">
        		<input value="" type="text" class="input-xlarge" name="name" id="add">
       			<button type="submit" class="btn btn-primary">Добавить</button>
     		</div>
    	</div>
  	</fieldset>
</form>