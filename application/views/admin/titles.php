<script>
	function edit_title(id) 
	{
		$('#title').attr('value', $('#title'+id).html());
		$('#controller').attr('value', $('#controller'+id).html());
		$('#description').attr('value', $('#description'+id).html());
		$('#action').attr('value', $('#action'+id).html());
		$('#active').attr('value', $('#active'+id).html());
		$('#id').attr('value', id);
		
		$('#formModal').attr('action', '/admin/editTitle/');
		$('#btnModal').html('Сохранить');
		
		$('#myModal').modal();
	}
	
	function add_title() 
	{
		$('#formModal').attr('action', '/admin/addTitle/');
		$('#btnModal').html('Добавить');
		
		$('#myModal').modal();
	}
</script>

<div class="modal hide" id="myModal">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">×</button>
    <h3>Редактировать страницу</h3>
  </div>
  <form id="formModal" action="" method="post">
  	<div class="modal-body">
  		<input id="id" name="id" type="hidden">
  		<label>Контроллер</label>
  		<input id="controller" name="controller" type="text" class="span3">
  		<label>Действие</label>
  		<input id="action" name="action" type="text" class="span3">
  		<label>Заголовок</label>
  		<input id="title" name="title" type="text" class="span3">
  		<label>Активность (0/1)</label>
  		<input id="active" name="active" type="text" class="span3">
  		<label>Описание</label>
  		<input id="description" name="description" type="text" class="span3">
 	</div>
  	<div class="modal-footer">
    	<button class="btn" data-dismiss="modal">Отмена</a>
    	<button id="btnModal" type="submit" class="btn btn-primary">Сохранить</a>
  	</div>
  </form>
</div>

<h3>Управление заголовками сайта</h3>
<? if ($titles): ?>
<table class="table">
	<thead>
    	<tr>
      		<th>Контроллер</th>
      		<th>Действие</th>
      		<th>Активность</th>
      		<th>Заголовок</th>
    	</tr>
  	</thead>
  	
  	<tbody>
  		<? foreach ($titles as $element): ?>
    	<tr onclick="edit_title(<?=$element['id']?>); return false">
      		<td id="controller<?=$element['id']?>"><?=$element['controller']?></td>
      		<td id="action<?=$element['id']?>"><?=$element['action']?></td>
      		<td id="active<?=$element['id']?>"><?=$element['active']?></td>
      		<td id="title<?=$element['id']?>"><?=$element['title']?></td>
      		<div class="hide" id="description<?=$element['id']?>"><?=$element['description']?></div>
   	 	</tr>
   	 	<? endforeach; ?>
  	</tbody>
</table>

<? endif; ?>

<div class="span6">
	<a onclick="add_title(); return false">Добавить заголовок</a><br/><br/>
	Подсказка: нажмите на строку для редактирования.
</div>

