<script>
	function edit_static(id) 
	{
		$('#title').attr('value', $('#title'+id).html());
		$('#active').attr('value', $('#active'+id).html());
		$('#text').attr('value', $('#text'+id).html());
		$('#id').attr('value', id);
		
		$('#formModal').attr('action', '/admin/editStatic/');
		$('#btnModal').html('Сохранить');
		/*
		if (id <= 6)
		{
			$('#title').prop('disabled', true);
			$('#active').prop('disabled', true);
		}
		else
		{
			$('#title').prop('disabled', false);
			$('#active').prop('disabled', false);			
		}
		*/
		$('#myModal').modal();
	}
	
	function add_static() 
	{
		$('#formModal').attr('action', '/admin/addStatic/');
		$('#btnModal').html('Добавить');
		
		$('#myModal').modal();
	}
</script>

<div class="modal hide" id="myModal">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">×</button>
    <h3>Редактировать страницу</h3>
  </div>
  <form id="formModal" action="" method="post">
  	<div class="modal-body">
  		<input id="id" name="id" type="hidden">
  		<label>Заголовок</label>
  		<input id="title" name="title" type="text" class="span3">
  		<label>Активность (0/1)</label>
  		<input id="active" name="active" type="text" class="span3">
  		<label>HTML-код</label>
  		<textarea rows="4" id="text" name="text" class="input-xlarge"></textarea>
 	</div>
  	<div class="modal-footer">
    	<button class="btn" data-dismiss="modal">Отмена</a>
    	<button id="btnModal" type="submit" class="btn btn-primary">Сохранить</a>
  	</div>
  </form>
</div>

<h3>Статические страницы и тексты</h3>
<? if ($static): ?>
<table class="table">
	<thead>
    	<tr>
    		<th>ID</th>
      		<th>Заголовок</th>
      		<th>Активность</th>
    	</tr>
  	</thead>
  	
  	<tbody>
  		<? foreach ($static as $element): ?>
    	<tr onclick="edit_static(<?=$element['id']?>); return false">
      		<td><?=$element['id']?></td>
      		<td id="title<?=$element['id']?>"><?=$element['title']?></td>
      		<td id="active<?=$element['id']?>"><?=$element['active']?></td>
      		<div class="hide" id="text<?=$element['id']?>"><?=$element['text']?></div>
   	 	</tr>
   	 	<? endforeach; ?>
  	</tbody>
</table>
<? endif; ?>

<div class="span6">
	<a onclick="add_static(); return false">Добавить страницу</a><br/><br/>
	Подсказка: нажмите на строку для редактирования.
</div>

