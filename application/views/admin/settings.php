<h3>Глобальные настройки сайта</h3>
<? foreach($settings as $setting): ?>
<form action="/admin/editsetting" method="post" class="form-horizontal">
	<fieldset>
    	<div class="control-group">
      		<label class="control-label" for="<?=$setting['name']?>"><?=$setting['name']?></label>
      		<div class="controls">
      			<input type="hidden" class="input-xlarge" name="name" value="<?=$setting['name']?>">
        		<input value="<?=$setting['value']?>" type="text" class="input-xlarge" name="value" id="<?=$setting['name']?>">
       			<button type="submit" class="btn btn-primary">Сохранить</button>
     		</div>
    	</div>
  	</fieldset>
</form>
<? endforeach; ?>