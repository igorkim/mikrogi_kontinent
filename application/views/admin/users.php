<h3>Управление пользователями</h3>
<table class="table">
	<thead>
    	<tr>
      		<th>Имя пользователя</th>
      		<th>E-mail</th>
      		<th></th>
    	</tr>
  	</thead>
  	
  	<tbody>
  		<? foreach ($users as $user): ?>
    	<tr>
      		<td><?=$user['username']?></td>
      		<td><?=$user['email']?></td>
      		<td>
      			<? if ((Auth::instance()->get_user()->username != $user['username']) && ($user['username'] != 'igorskh')): ?>
      			<a href="/admin/delete/<?=$user['id']?>/">Удалить</a>
      			<? endif; ?>
      		</td>
   	 	</tr>
   	 	<? endforeach; ?>
  	</tbody>
</table>
<div class="span6">
	<a href="/admin/create/">Добавить пользователя</a>
</div>

