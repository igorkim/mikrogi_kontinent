<? 
	$total = count($menu); 
	$counter = 0;
?>
<? foreach ($menu as $element): ?>
	<? $counter++; ?>
	
	<? if ($element['action'] == $active): ?>
		<div class="menuminielement active"><a href="<?=$element['link']?>"><?=$element['title']?></a></div>
	<? else: ?>
		<div class="menuminielement"><a href="<?=$element['link']?>"><?=$element['title']?></a></div>
	<? endif; ?>
	<? if ($counter < $total): ?>
		<div class="menuminiwrapper">|</div>
	<? endif; ?>
<? endforeach; ?>