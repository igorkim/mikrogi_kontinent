<!-- Item Container -->
<div class="bigitem">
	<!-- Product Picture -->
	<? if ($img == ''): ?>
	<img src="/img/product.png"/>
	<? else: ?>
	<img src="/img/<?=$img?>"/>
	<? endif; ?>
	<!-- Product Title -->
	<div class="name"><?=$title?></div>
	<!-- Product Description -->
	<div class="description">&nbsp;</div>
			
	<div class="fulldescription">
		<!-- Product Table -->
		<?=$table?>
	</div>
	<p><?=$description?></p>
</div>